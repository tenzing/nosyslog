#include <syslog.h>
#include <stdio.h>
#include <stdarg.h>

/* Defaults */
static const char *g_ident	= NULL;
static int g_facility		= LOG_USER;

/* Facility strings */
static inline const char *facility_string(int facility)
{
	switch (facility) {
#define X(F)	case LOG_##F: return #F
		X(AUTH);
		X(AUTHPRIV);
		X(CRON);
		X(DAEMON);
		X(FTP);
		X(KERN);
		X(LOCAL0);
		X(LOCAL1);
		X(LOCAL2);
		X(LOCAL3);
		X(LOCAL4);
		X(LOCAL5);
		X(LOCAL6);
		X(LOCAL7);
		X(LPR);
		X(MAIL);
		X(NEWS);
		X(SYSLOG);
		X(USER);
		X(UUCP);
#undef X
	}

	return "(unknown)";
}

/* Priority strings */
static inline const char *priority_string(int priority)
{
	switch (priority) {
#define X(P)	case LOG_##P: return #P
		X(EMERG);
		X(ALERT);
		X(CRIT);
		X(ERR);
		X(WARNING);
		X(NOTICE);
		X(INFO);
		X(DEBUG);
#undef X
	}

	return "(unknown)";
}

/* Overload the system openlog function */
void openlog(const char *ident, int option, int facility)
{
	(void) option;

	g_ident		= ident;
	g_facility	= facility;
}

/* Overload the system closelog function */
void closelog(void)
{
	/* nop */
}

/* Overload the system vsyslog function */
void vsyslog(int priority, const char *format, va_list ap)
{
	/* Output the facility and priority */
	fputs("[", stderr);
	fputs(facility_string(g_facility), stderr);
	fputs("] [", stderr);
	fputs(priority_string(priority), stderr);
	fputs("] ", stderr);

	/* Output the prefix string, if given */
	if (g_ident) {
		fputs(g_ident, stderr);
		fputs(": ", stderr);
	}

	/* Output the user message */
	vfprintf(stderr, format, ap);

	/* Output a new line */
	fputc('\n', stderr);
}

/* Overload the system syslog function */
void syslog(int priority, const char *format, ...)
{
	va_list ap;

	va_start(ap, format);
	vsyslog(priority, format, ap);
	va_end(ap);
}

