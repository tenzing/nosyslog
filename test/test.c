#include <syslog.h>

int main(int ac, char **av)
{
	openlog("prefix", 0, LOG_DAEMON);

	while (--ac > 0)
		syslog(LOG_WARNING, *++av);

	closelog();

	return 0;
}
