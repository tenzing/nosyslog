# Installation path
PREFIX	= /usr/local

# Files
NAME	= nosyslog
BIN	= $(NAME)
LIB	= lib$(NAME)-preload.so
SRCS	= $(wildcard *.c)
OBJS	= $(SRCS:.c=.o)

# Flags
CFLAGS	= -O2 -fPIC -Wall -Wextra

# Rules
all:	$(BIN) $(LIB)

$(BIN):	$(BIN).in
	sed s,%LIBPATH%,$(PREFIX)/lib/$(LIB), < $< > $@

$(LIB):	$(OBJS)
	$(CC) -shared -o $@ $^

%.o:	%.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	$(RM) $(BIN) $(LIB) $(OBJS)

install: $(BIN) $(LIB)
	install -m 0755 $(BIN) $(PREFIX)/bin/$(BIN)
	install -m 0644 $(LIB) $(PREFIX)/lib/$(LIB)

.PHONY:	all clean install
